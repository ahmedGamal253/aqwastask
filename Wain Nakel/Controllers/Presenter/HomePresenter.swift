//
//  HomePresenter.swift
//  Wain Nakel
//
//  Created by Admin on 10/9/19.
//  Copyright © 2019 Tasks. All rights reserved.
//

import Foundation
class HomePresenter{
    
    func getSuggestResturant (Ulat:Double,Ulng:Double,successBlock:@escaping (_ model :ResturentResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        ServiceHandler.sharedInstance.executeGetServiceWith(url: AppUrl.instance.suggestUrl(lat:Ulat, lng: Ulng), successBlock: { (response) in
            let model = ResturentResponseModel(response)
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    
}
