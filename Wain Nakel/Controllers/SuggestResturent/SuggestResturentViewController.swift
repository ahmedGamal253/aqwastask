//
//  SuggestResturentViewController.swift
//  Wain Nakel
//
//  Created by Admin on 10/10/19.
//  Copyright © 2019 Tasks. All rights reserved.
//

import UIKit
import MapKit
import NVActivityIndicatorView

class SuggestResturentViewController: UIViewController {

    @IBOutlet weak var labelSuggest: UILabel!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var btnSuggest: UIButton!
    @IBOutlet weak var labelCat: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var mapResturent: MKMapView!
    
    var resturent = ResturentResponseModel()
    var UserLat:Double?
    var UserLng:Double?
    let locationManager = CLLocationManager()
    
    init(Resturent:ResturentResponseModel) {
        resturent = Resturent
        super.init(nibName: "SuggestResturentViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUi()
        getUserLocation()
        setData()
        initMap()
        // Do any additional setup after loading the view.
    }

    func initUi(){
         btnSuggest.layer.cornerRadius = 25
    }
    
    func setData(){
        labelName.text = resturent.name
        labelCat.text = "\(resturent.cat) - \(resturent.rating)/10"
    }
    
    func initMap() {
        // map camera
        let mapCenter = CLLocationCoordinate2DMake(Double(resturent.lat) ?? 0, Double(resturent.lon) ?? 0)
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: mapCenter, span: span)
        mapResturent.region = region
        // add pin
        let annotation = MKPointAnnotation()
        let centerCoordinate = CLLocationCoordinate2D(latitude: Double(resturent.lat) ?? 0, longitude:Double(resturent.lon) ?? 0)
        annotation.coordinate = centerCoordinate
        annotation.title = resturent.name
        mapResturent.addAnnotation(annotation)
        
    }
    
    func getData(){
        labelSuggest.isHidden = true
        indicator.startAnimating()
        HomePresenter().getSuggestResturant(Ulat: UserLat ?? 0, Ulng: UserLng ?? 0, successBlock: { (model) in
            self.labelSuggest.isHidden = false
            self.indicator.stopAnimating()
            self.resturent = model
            self.setData()
            self.initMap()
        }) { (error) in
            self.labelSuggest.isHidden = false
            self.showAlart(message: error)
        }
    }
    
    @IBAction func suggest(_ sender: Any) {
        getData()
    }
    
}
