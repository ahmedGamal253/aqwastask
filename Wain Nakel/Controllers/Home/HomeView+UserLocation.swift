//
//  HomeView+UserLocation.swift
//  Wain Nakel
//
//  Created by Admin on 10/10/19.
//  Copyright © 2019 Tasks. All rights reserved.
//

import Foundation
import CoreLocation

extension HomeViewController:CLLocationManagerDelegate{
    
    func getUserLocation(){
       // CLLocationManager.locationServicesEnabled()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        UserLat = locValue.latitude
        UserLng = locValue.longitude
    }
}
