//
//  AppUrl.swift
//  Yacoun
//
//  Created by User on 11/6/17.
//  Copyright © 2017 Internet Plus. All rights reserved.
//

import Foundation

class AppUrl {
    
    static let instance = AppUrl()
    private init() { }
    
    let baseUrl = "https://wainnakel.com/api/v1/GenerateFS.php?"

    func suggestUrl(lat: Double, lng: Double) -> String {
        return (baseUrl + "uid=\(lat),\(lng)")
    }
    
}

