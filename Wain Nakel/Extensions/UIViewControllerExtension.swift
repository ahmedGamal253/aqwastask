//
//  UIViewControllerExtension.swift
//  Pharmacist
//
//  Created by mohamed elmaazy on 7/9/18.
//  Copyright © 2018 Ahmed gamal. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func getRatio(_ number: CGFloat) -> CGFloat {
        let ratio = CGFloat(Int(number * UIScreen.main.bounds.width / 360))
        return ratio > 0 ? ratio : CGFloat(number * UIScreen.main.bounds.width / 360)
    }
    
    func showAlart(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "حسنا", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

