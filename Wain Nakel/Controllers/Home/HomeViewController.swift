//
//  ViewController.swift
//  Wain Nakel
//
//  Created by Admin on 10/9/19.
//  Copyright © 2019 Tasks. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import Hero

class HomeViewController: UIViewController {

    @IBOutlet weak var indicator: NVActivityIndicatorView!
    @IBOutlet weak var constrainWidthBtnSuggest: NSLayoutConstraint!
    @IBOutlet weak var constrainBottomBtnSuggest: NSLayoutConstraint!
    @IBOutlet weak var btnSuggest: UIButton!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var constrainCenterVerticalLogo: NSLayoutConstraint!
    
    var UserLat:Double?
    var UserLng:Double?
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         animatView()
         getUserLocation()
    }

    func animatView(){
        UIView.animate(
            withDuration: 1.5,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1.2,
            options: .curveEaseInOut,
            animations: {
                self.constrainCenterVerticalLogo.constant = -110
                self.constrainWidthBtnSuggest.constant = self.getRatio(300)
                self.constrainBottomBtnSuggest.constant = self.getRatio(100)
                self.btnSuggest.layer.cornerRadius = 25
                self.view.layoutIfNeeded()
                
        }, completion: nil)
    }

    
    @IBAction func suggest(_ sender: Any) {
       btnSuggest.setTitle("", for: .normal)
       indicator.startAnimating()
        HomePresenter().getSuggestResturant(Ulat: UserLat ?? 0, Ulng: UserLng ?? 0, successBlock: { (model) in
            self.indicator.stopAnimating()
            self.GoNext(model: model)
        }) { (error) in
            self.showAlart(message: error)
        }
    }
 
    func GoNext(model: ResturentResponseModel) {
        let vc = SuggestResturentViewController(Resturent: model)
        let nav = UINavigationController(rootViewController: vc)
        vc.hero.isEnabled = true
        nav.hero.isEnabled = true
        nav.navigationBar.isHidden = true
        present(nav, animated: true, completion: nil)
    }
}

