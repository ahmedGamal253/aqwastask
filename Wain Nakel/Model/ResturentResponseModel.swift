//
//  ResturentModel.swift
//  Wain Nakel
//
//  Created by Admin on 10/9/19.
//  Copyright © 2019 Tasks. All rights reserved.
//

import Foundation
class ResturentResponseModel{
    var error = ""
    var name = ""
    var id = ""
    var link = ""
    var cat = ""
    var catId = ""
    var rating = ""
    var lat = ""
    var lon = ""
    var Ulat = ""
    var Ulon = ""
    var open = ""
    var image = [String]()
    
    init(_ response:[String:Any] = [String:Any]()) {
        let dic = HandleJSON.instance.handle(dicc: response)
        error = dic["error"] as? String ?? ""
        name = dic["name"] as? String ?? ""
        id = dic["id"] as? String ?? ""
        link = dic["link"] as? String ?? ""
        cat = dic["cat"] as? String ?? ""
        catId = dic["catId"] as? String ?? ""
        rating = dic["rating"] as? String ?? ""
        lat = dic["lat"] as? String ?? ""
        lon = dic["lon"] as? String ?? ""
        Ulat = dic["Ulat"] as? String ?? ""
        Ulon = dic["Ulon"] as? String ?? ""
        open = dic["open"] as? String ?? ""
        image = dic["image"] as? [String] ?? [String]()
    }
}
