//
//  ServieHandler.swift
//  Mirsal_Swift
//
//  Created by Ibrahim on 1/8/19.
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import Foundation
import Alamofire

class ServiceHandler {
    
    static let sharedInstance = ServiceHandler()
    
    private func checkConnection() -> Bool {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        return (reachabilityManager?.isReachable)!
    }
    
    func executeGetServiceWith(url:String, successBlock: @escaping (_ response:[String: Any]) -> Void,
                               errorBlock: @escaping (_ errorResponse:String) -> Void)  {
        if checkConnection() {
            Alamofire.request(url, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if let jsonDict = response.result.value as? Dictionary<String, Any> {
                        if jsonDict["error"] as! String == "no" {
                            successBlock(jsonDict)
                        }else {
                            errorBlock("خظا فى الحصول البيانات")
                        }
                    }
                    break
                case .failure(let error):
                    if !(self.checkConnection()) {
                        errorBlock("خطا فى الاتصال بالانترنت")
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print(error.localizedDescription)
                        errorBlock("خطا فى الاتصال بالانترنت")
                    }else {
                         errorBlock(error.localizedDescription)
                    }
                }
            }
        } else {
           errorBlock("خطا فى الاتصال بالانترنت")
        }
    }

}
